<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>onetech</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <!-- Load Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script%7CPoppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href="css/basic.css" />
    <link rel="stylesheet" href="css/layout.css" />
    <link rel="stylesheet" href="css/blogs.css" />
    <link rel="stylesheet" href="css/ionicons.css" />
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <link rel="stylesheet" href="css/animate.css" />

    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicons/favicon.ico">


</head>

<body>

<!-- Page -->
<div class="page">

    <!-- Preloader -->
    <div class="preloader">
        <div class="centrize full-width">
            <div class="vertical-center">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
    </div>

    <header class="header">
        <div class="fw">
            <div class="logo">
                <a href="#"><img src="images/onetech.jpg" width="140px" height="140px"/></a>
            </div>
            <a href="#" class="menu-btn"><span></span></a>
            <div class="top-menu">
                <ul>
                    <li><a href="#about-section">About</a></li>
                    <li><a href="#services-section">What I Do</a></li>
                    <li><a href="#works-section">Works</a></li>
                    <li><a href="#blog-section">Blog</a></li>
                    <li><a href="#contact-section">Contact</a></li>

                </ul>
                <a href="#" class="close"></a>
            </div>
        </div>
    </header>

    <!-- Container -->
    <div class="container">

        <!-- Section Started -->
        <div class="section started">
            <div class="slide" style="background-image: url(images/slide-bg-1.jpg);"></div>
            <div class="centrize full-width">
                <div class="vertical-center">
                    <div class="st-title align-center">
                        <div class="typing-title">
                            <p>Software Engineer</p>
                            <p>DevOps Engineer</p>
                            <p>Systems Engineer</p>
                            <p>Security Engineer</p>
                            <p>Consultant</p>
                            <p>Developer</p>
                            <p>Coder</p>

                       </div>
                        <span class="typed-title"></span>
                    </div>
                    <div class="socials">
                        <a target="blank" href="https://www.facebook.com"><i class="icon ion ion-social-facebook"></i></a>
                        <a target="blank" href="https://github.com"><i class="icon ion ion-social-github"></i></a>

                    </div>

                </div>
            </div>
            <a href="#" class="mouse-btn"><i class="icon ion ion-chevron-down"></i></a>

        </div>

        <!-- Section About -->
        <div class="section about align-left" id="about-section">
            <div class="fw">
                <div class="row">
                    <div class="col col-m-12 col-t-4 col-d-4">
                        <div class="profile">
                            <img src="images/profile.jpg" alt="" />
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-8 col-d-8">
                        <div class="text-box">
                            <h1>I'm Nana Kwafo Mensah, Computer Engineer in leeds,United Kingdom.</h1>
                            <p>
                                I am a highly skilled computer Engineer with wealth of experience in Software Engineering, DevOps Engineering, MobileApplication Development (Apache Cordova), USSD Applications Development, WebApplication Development, Linus Systems Configuration & Administration, Networking, windows server 2012.
                            </p>
                            <p>
                                I holds a BSc. (Hons) in Computer Science and Engineering from the University of Mines. Prior to starting this blog, I have worked with nalosolutions Ghana and ITE Ghana and Supertech Ghana.
                            </p>


                        </div>
                        {{--<div class="bts">--}}
                            {{--<a href="#" class="btn extra">Download Resume</a>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
        <table>
            <tr>
                <td style="background-color: red"></td>
                <td style="background-color: yellow"></td>
                <td style="background-color: green"></td>
            </tr>
        </table>
        <!-- Section Services -->
        <div class="section services gray align-left" id="services-section">
            <div class="fw">
                <div class="titles">
                    <div class="title">What I Do</div>
                </div>
                <div class="row">

                    <div class="col col-m-12 col-t-6 col-d-4">
                        <div class="service-item">
                            <div class="circle"><i class="icon ion-ios-grid-view-outline"></i></div>
                            <div class="name">Web Applications</div>
                            <p>A computer program that performs a specific function by using a web browser as its client</p>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-4">
                        <div class="service-item">
                            <div class="circle"><i class="icon ion ion-social-android-outline"></i></div>
                            <div class="name">Mobile/USSD Apps</div>
                            <p>A  computer program that performs a specific function by using a mobile phone as its client</p>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-4">
                        <div class="service-item">
                            <div class="circle"><i class="icon ion ion-ios-analytics-outline"></i></div>
                            <div class="name">DevOps Enigineering</div>
                            <p>An Engineering Process which includes system administration,Deploy virtualization and Networking</p>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-4">
                        <div class="service-item">
                            <div class="circle"><i class="icon ion ion-ios-browsers-outline"></i></div>
                            <div class="name">Windows Server Administration</div>
                            <p> an advanced computer networking topic that includes server installation and configuration, server roles, storage, Active Directory and Group Policy, file, print,
                                and web services, remote access, virtualization, application servers, troubleshooting, performance, and reliability.</p>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-4">
                        <div class="service-item">
                            <div class="circle"><i class="icon ion ion-ios-barcode-outline"></i></div>
                            <div class="name">Linus/Web Server Administration</div>
                            <p>Installation and configurations of applications on a linus server</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Portfolio -->
        <div class="section works align-left" id="works-section">
            <div class="fw">
                <div class="titles">
                    <div class="title">Works</div>
                </div>
                <div class="filters">
                    <div class="f_btn active">
                        <label><input type="radio" name="fl_radio" value="box-item" />All</label>
                    </div>
                    <div class="f_btn">
                        <label><input type="radio" name="fl_radio" value="f-personal" />Personal</label>
                    </div>
                    <div class="f_btn">
                        <label><input type="radio" name="fl_radio" value="f-companys" />With Company's</label>
                    </div>
                    {{--<div class="f_btn">--}}
                        {{--<label><input type="radio" name="fl_radio" value="f-ui" />UI Kits</label>--}}
                    {{--</div>--}}
                    {{--<div class="f_btn">--}}
                        {{--<label><input type="radio" name="fl_radio" value="f-photo" />Photography</label>--}}
                    {{--</div>--}}
                </div>
                <div class="row box-items">
                    <div class="col col-m-12 col-t-6 col-d-4 box-item f-personal">
                        <div class="item">
                            <div class="desc">
                                <div class="image">
                                    <a href="#popup-1" class="has-popup"><img src="images/works/work1.jpg" alt="" /></a>
                                </div>
                            </div>
                        </div>
                        <div id="popup-1" class="popup-box mfp-fade mfp-hide">
                            <div class="content">
                                <div class="image">
                                    <img src="images/works/work1.jpg" alt="">
                                </div>
                                <div class="desc">
                                    <div class="category">companys</div>
                                    <h4>Crude Oil Processing Tool</h4>
                                    <p>
                                       A web application tool that was built for dmt collateral company for processing crude oil from vessels at depot
                                    </p>
                                    <a href="#" class="btn">View Project</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-4 box-item f-companys">
                        <div class="item">
                            <div class="desc">
                                <div class="image">
                                    <a href="#popup-2" class="has-popup"><img src="images/works/work2.jpg" alt="" /></a>
                                </div>
                            </div>
                        </div>
                        <div id="popup-2" class="popup-box mfp-fade mfp-hide">
                            <div class="content">
                                <div class="image">
                                    <img src="images/works/work2.jpg" alt="">
                                </div>
                                <div class="desc">
                                    <div class="category">Companys</div>
                                    <h4>Glico insurance ERP tool</h4>
                                    <p>
                                        Together with nalosolutions team ,this web application tool was built for glico insurance company for managing insurance policy for their client
                                    </p>
                                    <a href="#" class="btn">View Project</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-4 box-item f-personal">
                        <div class="item">
                            <div class="desc">
                                <div class="image">
                                    <a href="#popup-3" class="has-popup"><img src="images/works/work3.jpg" alt="" /></a>
                                </div>
                            </div>
                        </div>
                        <div id="popup-3" class="popup-box mfp-fade mfp-hide">
                            <div class="content">
                                <div class="image">
                                    <img src="images/works/work3.jpg" alt="">
                                </div>
                                <div class="desc">
                                    <div class="category">Personal</div>
                                    <h4>Church management System</h4>
                                    <p>
                                        Developed this entire software to manage all the enteire activities in ICGC church in Ghana
                                    </p>
                                    <a href="#" class="btn">View Project</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-4 box-item f-companys">
                        <div class="item">
                            <div class="desc">
                                <div class="image">
                                    <a href="#popup-4" class="has-popup"><img src="images/works/work4.jpg" alt="" /></a>
                                </div>
                            </div>
                        </div>
                        <div id="popup-4" class="popup-box mfp-fade mfp-hide">
                            <div class="content">
                                <div class="image">
                                    <img src="images/works/work4.jpg" alt="">
                                </div>
                                <div class="desc">
                                    <div class="category">Branding</div>
                                    <h4>Courier Services</h4>
                                    <p>
                                        Together with nalosolutions team ,this web and ussd application  was designed  for courier service for managing client request by dialing a shortcode
                                    </p>
                                    <a href="#" class="btn">View Project</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-4 box-item f-companys">
                        <div class="item">
                            <div class="desc">
                                <div class="image">
                                    <a href="#popup-5" class="has-popup"><img src="images/works/work5.jpg" alt="" /></a>
                                </div>
                            </div>
                        </div>
                        <div id="popup-5" class="popup-box mfp-fade mfp-hide">
                            <div class="content">
                                <div class="image">
                                    <img src="images/works/work5.jpg" alt="">
                                </div>
                                <div class="desc">
                                    <div class="category">Photography</div>
                                    <h4>Investor plus</h4>
                                    <p>
                                        Together with nalosolutions team ,this web and mobile application  was designed  as an ERP system for GIPC TO manageall activities and process of investors that come into the countru of Ghana

                                    </p>
                                    <a href="#" class="btn">View Project</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-4 box-item f-companys">
                        <div class="item">
                            <div class="desc">
                                <div class="image">

                                    <a href="#popup-6" class="has-popup"><img src="images/works/work6.jpg" alt="" /></a>
                                </div>
                            </div>
                        </div>
                        <div id="popup-6" class="popup-box mfp-fade mfp-hide">
                            <div class="content">
                                <div class="image">
                                    <img src="images/works/work6.jpg" alt="">
                                </div>
                                <div class="desc">
                                    <div class="category">Companys</div>
                                    <h4>Stocktape</h4>
                                    <p>
                                       Together with wonderbyte team designed  a web and mobile based application for processing and getting instant reporting on activities in the oila and gas sector
                                    </p>
                                    <a href="stocktape.info" class="btn">View Project</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-4 box-item f-companys">
                        <div class="item">
                            <div class="desc">
                                <div class="image">

                                    <a href="#popup-7" class="has-popup"><img src="images/works/work7.jpg" alt="" /></a>
                                </div>
                            </div>
                        </div>
                        <div id="popup-7" class="popup-box mfp-fade mfp-hide">
                            <div class="content">
                                <div class="image">
                                    <img src="images/works/work7.jpg" alt="">
                                </div>
                                <div class="desc">
                                    <div class="category">Company</div>
                                    <h4>Banqmoni</h4>
                                    <p>
                                       This is a ussd application that was built with ite team for the purpose of direct transfer of mony from mobile money wallet to banks
                                       Service currently not available
                                    </p>
                                    <a href="#" class="btn">View Project</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-4 box-item f-companys">
                        <div class="item">
                            <div class="desc">
                                <div class="image">

                                    <a href="#popup-8" class="has-popup"><img src="images/works/work8.jpg" alt="" /></a>
                                </div>
                            </div>
                        </div>
                        <div id="popup-8" class="popup-box mfp-fade mfp-hide">
                            <div class="content">
                                <div class="image">
                                    <img src="images/works/work8.jpg" alt="">
                                </div>
                                <div class="desc">
                                    <div class="category">Company</div>
                                    <h4>Pasco</h4>
                                    <p>
                                        This is an application that help students with past questions by dialing a shortcode.
                                        Service currently not available
                                    </p>
                                    <a href="#" class="btn">View Project</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-4 box-item f-companys">
                        <div class="item">
                            <div class="desc">
                                <div class="image">

                                    <a href="#popup-9" class="has-popup"><img src="images/works/work9.jpg" alt="" /></a>
                                </div>
                            </div>
                        </div>
                        <div id="popup-9" class="popup-box mfp-fade mfp-hide">
                            <div class="content">
                                <div class="image">
                                    <img src="images/works/work9.jpg" alt="">
                                </div>
                                <div class="desc">
                                    <div class="category">UI Kits</div>
                                    <h4>Loyalty Insurance</h4>
                                    <p>
                                       Together with ITE team ,designed a ussd app for Loyalty Inuarance for rewarding their clients
                                    </p>
                                    <a href="#" class="btn">View Project</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-6 col-d-4 box-item f-personal">
                        <div class="item">
                            <div class="desc">
                                <div class="image">

                                    <a href="#popup-10" class="has-popup"><img src="images/works/work10.jpg" alt="" /></a>
                                </div>
                            </div>
                        </div>
                        <div id="popup-10" class="popup-box mfp-fade mfp-hide">
                            <div class="content">
                                <div class="image">
                                    <img src="images/works/work10.jpg" alt="">
                                </div>
                                <div class="desc">
                                    <div class="category">Personal</div>
                                    <h4>ESYN</h4>
                                    <p>
                                        </p>
                                    <a href="#" class="btn">View Project</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        <!-- Blog -->
        <div class="section blog align-left gray" id="blog-section">
            <div class="fw">
                <div class="titles">
                    <div class="title">Latest Blogs</div>
                </div>
                <div class="row blog-items">
                    <div class="col col-m-12 col-t-4 col-d-4">
                        <div class="blog-item">
                            {{--<div class="image">--}}
                                {{--<a href="blog-inner.html"><img src="images/blog/blog1.png" alt="" /></a>--}}
                            {{--</div>--}}
                            <a href="devop1" class="name">Setting up the environment for hosting your spring boot Application on Ubuntu</a>
                            <p>Once you the developer is done developing your spring boot application,its now time
                                to host your application so that it can be accessed publicly.</p>
                            <div class="date">Kwafo Nana Mensah - 2 September 2018</div>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-4 col-d-4">
                        <div class="blog-item">
                            {{--<div class="image">--}}
                                {{--<a href="blog-inner.html"><img src="images/blog/blog2.png" alt="" /></a>--}}
                            {{--</div>--}}
                            <a href="devop2" class="name">Installing and Configuring nginx on Ubuntu 16.04.4 LTS</a>
                            <p>Nginx is a web server. what then is a webserver?  A  web server is a server that hosts an application that listens to the HTTP requests .There are a few web servers around,</p>
                            <div class="date">Kwafo Nana Mensah - 30 August 2018</div>
                        </div>
                    </div>
                    <div class="col col-m-12 col-t-4 col-d-4">
                        <div class="blog-item">
                            {{--<div class="image">--}}
                                {{--<a href="blog-inner.html"><img src="images/blog/blog3.png" alt="" /></a>--}}
                            {{--</div>--}}
                            <a href="devop3" class="name">Enabling a Port after configuring virtual host on nginx webserver</a>
                            <p>Say you have finished setting up your  virtual host on nginx on a particular port say xxx.xxx.xxx.xxx:83. This cant cannot be accessed on host browser till  a firewall is added that will enable port 83.</p>
                            <div class="date">Kwafo Nana Mensah - 12 October 2018</div>
                        </div>
                    </div>
                </div>
                <div class="bts">
                    <a href="blog" class="btn extra">View Blog</a>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        <!-- Section Contacts -->
        <div class="section contacts align-left" id="contact-section">
            <div class="fw">
                <div class="titles">
                    <div class="title">Contact Me</div>
                </div>

                <div class="contact-form">

                    <form id="cform" action="{{ url('insert-ajax') }}" method="GET" onsubmit="return InsertViaAjax();" >

                        <div class="row">
                            <div class="col col-m-12 col-t-6 col-d-6">
                                <div class="value">
                                    <input type="text"id="name" name="name" placeholder="Name *" />
                                </div>
                            </div>
                            <div class="col col-m-12 col-t-6 col-d-6">
                                <div class="value">
                                    <input type="tel" id="phone" name="phone" placeholder="Phone *" />
                                </div>
                            </div>
                            <div class="col col-m-12 col-t-6 col-d-6">
                                <div class="value">
                                    <input type="text" id="email" name="email" placeholder="Email *" />
                                </div>
                            </div>
                            <div class="col col-m-12 col-t-6 col-d-6">
                                <div class="value">
                                    <input type="text" id="subject" name="subject" placeholder="Subject *" />
                                </div>
                            </div>
                            <div class="col col-m-12 col-t-12 col-d-12">
                                <div class="value">
                                    <textarea id="message" name="message" placeholder="Message"></textarea>
                                </div>
                            </div>
                            <div class="col col-m-12 col-t-12 col-d-12">
                                <button class="btn" >Send Message</button>
                            </div>
                        </div>
                    </form>
                    <div class="alert-success" id="success-message">
                       </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <footer class="align-center">
            <div class="socials">
                <a target="blank" href="https://www.facebook.com"><i class="icon ion ion-social-facebook"></i></a>
                <a target="blank" href="https://github.com"><i class="icon ion ion-social-github"></i></a>
                {{--<a target="blank" href="https://twitter.com"><i class="icon ion ion-social-twitter"></i></a>--}}
                {{--<a target="blank" href="https://www.youtube.com"><i class="icon ion ion-social-youtube"></i></a>--}}
                {{--<a target="blank" href="https://plus.google.com"><i class="icon ion ion-social-googleplus"></i></a>--}}
            </div>
            <div class="copy">© 2018 nanakwafomensah. all rights reserved.</div>
        </footer>
        <audio id="my_audio" src="bg.mp3" loop="loop"></audio>
    </div>

</div>

<!-- jQuery Scripts -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/masonry.pkgd.js"></script>
<script src="js/imagesloaded.pkgd.js"></script>
<script src="js/masonry-filter.js"></script>
<script src="js/magnific-popup.js"></script>
<script src="js/jquery.mb.YTPlayer.js"></script>
<script src="js/particles.js"></script>
<script src="js/typed.js"></script>

<!-- Main Scripts -->
<script src="js/main.js"></script>
<script>
    window.onload = function() {
        document.getElementById("my_audio").play();
    }
    function InsertViaAjax() {


        var form = $("#cform");



        var name = $("#name").val();
        var phone = $("#phone").val();
        var email = $("#email").val();
        var subject = $("#subject").val();
        var message = $("#message").val();

        //Run Ajax

        $.ajax({

            type:"GET",
            url:"{{url('insert-ajax')}}/"+name+"/"+phone+"/"+email+"/"+subject+"/"+message,
            success: function(data) {
                $("#success-message").html('<p>Thanks, your message is sent successfully. You will be contacted shortly!</p>');
            },
            beforeSend: function(){
                $("#success-message").html('<p>Sending message...</p>');
            }
        });

        // To Stop the loading of page after a button is clicked
        return false;
    }
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5bfa8d1479ed6453ccab053c/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>
</html>