<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>onetech</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <!-- Load Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script%7CPoppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href="css/basic.css" />
    <link rel="stylesheet" href="css/layout.css" />
    <link rel="stylesheet" href="css/blogs.css" />
    <link rel="stylesheet" href="css/ionicons.css" />
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <link rel="stylesheet" href="css/animate.css" />

    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicons/favicon.ico">

</head>

<body>

<!-- Page -->
<div class="page">

    <!-- Preloader -->
    <div class="preloader">
        <div class="centrize full-width">
            <div class="vertical-center">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
    </div>

    <header class="header">
        <div class="fw">


            <div class="logo">
                <a href="/">onetech</a>
            </div>
            <a href="#" class="menu-btn"><span></span></a>
            <div class="top-menu">
                <ul>
                    <li><a href="blog">Back to Posts</a></li>
                </ul>
                <a href="#" class="close"></a>
            </div>
        </div>
    </header>

    <!-- Container -->
    <div class="container">

        <!-- Section Started -->
        <div class="section started-blog">
            <div class="slide" style="background-image: url(images/slide-bg-1.jpg);"></div>
        </div>

        <!-- Section Blog -->
        <div class="section blog-single align-left" id="blog-section">
            <div class="fw">
                <div class="row">
                    <div class="bts" style="float: right;">
                        <a href="about" class="btn extra" style="font-size: larger">About the Engineer</a>
                    </div>
                    <div class="col col-m-12 col-t-12 col-d-12">

                        <div class="post-box">

                            <h1>Enabling a Port after configuring virtual host on nginx webserver</h1>
                            <div class="blog-detail">Posted <span>12 October 2018</span></div>

                            {{--<div class="blog-image">--}}
                            {{--<img src="images/blog/blog1.jpg" alt="">--}}
                            {{--</div>--}}

                            <div class="blog-content">
                                <p>Say you have finished setting up your  virtual host on nginx on a particular port say xxx.xxx.xxx.xxx:83. This cant cannot be accessed on host browser till  a firewall is added that will enable port 83.</p>
                                <p>
                                    When nginx a server is set up for the first time ,The Uncomplicated  firewall is normally inactive
                                    You can check the status of the firewall by using the command
                                    <blockquote>
                                    $sudo ufw status
                                    </blockquote>
                                    This should give you this results below
                                    <blockquote>
                                        # Status inactive
                                    </blockquote>
                               </p>
                                <p>
                                    In order to achieve our purpose today we will  follow the steps below

                                    Step 1   Enable the firewall

                                    Use the command:
                                <blockquote>
                                    sudo ufw enable
                                </blockquote>
                                    Step 2 Enable a port on the firewall with the command below
                                    <blockquote>
                                    sudo ufw allow 83
                                    </blockquote>
                                </p>
                                <p>Huray the port is now oppened for http connection</p>
                            </div>

                            <div class="col-md-12">
                                <h3 class="subtitle">Post Comments</h3>
                                <ul class="post-comment">
                                    @if(\App\Post::find($post_id)->comments->count() > 0)

                                        @foreach(\App\Post::find($post_id)->comments as $s)
                                            <li>
                                                <img src="images/person-1.jpg" alt="">
                                                <div class="comment-info">
                                                    <div class="name">
                                                        <h3>{{$s->name}}</h3>
                                                        <span>{{$s->created_at}}</span>
                                                        {{--<a href="#"> Reply </a>--}}
                                                    </div>
                                                    <p>{{$s->comment}}</p>
                                                </div>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>

                            <form  method="post" action="savecomment">
                                {{csrf_field()}}
                                <h3 class="subtitle">Leave a Reply</h3>
                                <div class="row">
                                    <div class="col col-m-12 col-t-12 col-d-6">
                                        <input type="hidden" name="post_id" value="{{$post_id}}"/>
                                        <input type="hidden" name="url" value="devop3"/>
                                        <div class="group-val">
                                            <input type="text" name="name" placeholder="Your Name" />
                                        </div>
                                        <div class="group-val">
                                            <input type="text" name="email" placeholder="Your Email" />
                                        </div>
                                        <div class="group-val ct-gr">
                                            <textarea name="comment" placeholder="Message"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn_animated" ><span class="circle">Send Message</span></button>
                                    </div>
                                </div>
                            </form>

                            @if(Session::has('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>Success!</strong> {{Session::get('success')}}
                                </div>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <footer class="align-center">
            <div class="socials">
                <a target="blank" href="https://www.facebook.com"><i class="icon ion ion-social-facebook"></i></a>
                <a target="blank" href="https://github.com"><i class="icon ion ion-social-github"></i></a>

            </div>
            <div class="copy">© 2018 onetech. all rights reserved.</div>
        </footer>

    </div>

</div>

<!-- jQuery Scripts -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/masonry.pkgd.js"></script>
<script src="js/imagesloaded.pkgd.js"></script>
<script src="js/masonry-filter.js"></script>
<script src="js/magnific-popup.js"></script>
<script src="js/jquery.mb.YTPlayer.js"></script>
<script src="js/particles.js"></script>
<script src="js/typed.js"></script>

<!-- Main Scripts -->
<script src="js/main.js"></script>

</body>
</html>