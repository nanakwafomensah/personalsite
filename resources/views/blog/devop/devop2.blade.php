<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>onetech</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <!-- Load Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script%7CPoppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href="css/basic.css" />
    <link rel="stylesheet" href="css/layout.css" />
    <link rel="stylesheet" href="css/blogs.css" />
    <link rel="stylesheet" href="css/ionicons.css" />
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <link rel="stylesheet" href="css/animate.css" />

    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicons/favicon.ico">

</head>

<body>

<!-- Page -->
<div class="page">

    <!-- Preloader -->
    <div class="preloader">
        <div class="centrize full-width">
            <div class="vertical-center">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
    </div>

    <header class="header">
        <div class="fw">

            <div class="logo">
                <a href="#">onetech</a>
            </div>
            <a href="#" class="menu-btn"><span></span></a>
            <div class="top-menu">
                <ul>
                    <li><a href="blog.html">Back to Posts</a></li>
                </ul>
                <a href="#" class="close"></a>
            </div>
        </div>
    </header>

    <!-- Container -->
    <div class="container">

        <!-- Section Started -->
        <div class="section started-blog">
            <div class="slide" style="background-image: url(images/slide-bg-1.jpg);"></div>
        </div>

        <!-- Section Blog -->
        <div class="section blog-single align-left" id="blog-section">
            <div class="fw">
                <div class="row">
                    <div class="bts" style="float: right;">
                        <a href="about" class="btn extra" style="font-size: larger">About the Engineer</a>
                    </div>
                    <div class="col col-m-12 col-t-12 col-d-12">

                        <div class="post-box">

                            <h1>Installing and Configuring nginx on Ubuntu 16.04.4 LTS</h1>
                            <div class="blog-detail">Posted <span>30 August 2018</span></div>
                            <div class="blog-content">
                                <p><b>Introduction</b></p>
                                <p>Nginx is a web server. what then is a webserver?  A  web server is a server that hosts an application that listens to the HTTP requests .There are a few web servers around, three dominate: Apache , Microsoft InternetInformation Services (IIS) , and Nginx combined have captured around 85 percent of the market.</p>

                                <p>
                                    <b>Why use Nginx</b>
                                </p>
                                <ul class="list-style">
                                    <li> it is fast</li>
                                    <li> it can accelerate your application</li>
                                    <li> it has a straight forward load Balancer</li>
                                    <li> it Scales well</li>
                                </ul>
                                <p><b>Pre-requisite</b></p>
                                <p>Before you begin this guide, you should have a regular, non-root user with sudo privileges configured on your server.</p>
                                <p><b>Step 1 : Install Nginx</b></p>
                                  <p>
                                    Since this is our first interaction with the apt packaging system in this session, we will update our local package index.  Afterwards, we can install nginx:

                                    Run the following commands</p>
                                <blockquote>
                                    $ sudo apt-get update<br>

                                    $ sudo apt-get install nginx
                                </blockquote>
                                <p>
                                    <b>Step 2  Adjust the firewall</b></p>
                                  <p>
                                    Nginx registers itself as a service with ufw, our firewall, upon installation.

                                    We can list the applications configurations that ufw knows how to work with by typing:
                                </p>
                                <blockquote>
                                    $ sudo ufw app list
                                </blockquote>
                                <p>you should get output like :</p>
                                <blockquote>
                                    Available Application<br>
                                    Nginx FULL<br>
                                    Nginx HTTP<br>
                                    Nginx HTTP<br>
                                    OpenSSH
                                </blockquote>
                                <p>
                                    As you can see, there are three profiles available for Nginx</br>

                                    Nginx Full: This profile opens both port 80 (normal, unencrypted web traffic) and port 443 (TLS/SSL encrypted traffic)</br>

                                    Nginx HTTP: This profile opens only port 80 (normal, unencrypted web traffic)</br>

                                    Nginx HTTPS: This profile opens only port 443 (TLS/SSL encrypted traffic)</br>
                                </p>
                                <p>You can enable this by typing:</p>
                                <blockquote>
                                    $ sudo ufw allow ‘Nginx HTTP’
                                </blockquote>
                                <p>You can verify the change by typing</p>
                                <blockquote>
                                    $ sudo ufw status
                                </blockquote>
                                <p>You should see HTTP traffic allowed in the displayed output</p>
                                <blockquote>
                                    Status: active
                                </blockquote>
                                <blockquote>
                                    <table>
                                        <tr>
                                            <td>To</td>
                                            <td>Action</td>
                                            <td>From</td>
                                        </tr>
                                        <tr>
                                            <td>OpenSSH</td>
                                            <td>ALLOW</td>
                                            <td>Anywhere</td>
                                        </tr>
                                        <tr>
                                            <td>Nginx HTTP</td>
                                            <td>ALLOW</td>
                                            <td>Anywhere</td>
                                        </tr>
                                        <tr>
                                            <td>OpenSSH (v6)</td>
                                            <td>ALLOW</td>
                                            <td>Anywhere(v6)</td>
                                        </tr>
                                        <tr>
                                            <td>Nginx HTTP (v6)</td>
                                            <td>ALLOW</td>
                                            <td>Anywhere(v6)</td>
                                        </tr>
                                    </table>
                                </blockquote>
                                <p><b>Step 3 Check your webserver</b></p>

                                   <p> check the status of your webserver</p>
                                <blockquote>$ service nginx status</blockquote>
                                <p>When you have your server’s IP address or domain, enter it into your browser’s address bar:

                                    http://server_domain_or_IP</p>
                                <div class="col-md-12">
                                    <h3 class="subtitle">Post Comments</h3>
                                    <ul class="post-comment">
                                        @if(\App\Post::find($post_id)->comments->count() > 0)

                                            @foreach(\App\Post::find($post_id)->comments as $s)
                                                <li>
                                                    <img src="images/person-1.jpg" alt="">
                                                    <div class="comment-info">
                                                        <div class="name">
                                                            <h3>{{$s->name}}</h3>
                                                            <span>{{$s->created_at}}</span>
                                                            {{--<a href="#"> Reply </a>--}}
                                                        </div>
                                                        <p>{{$s->comment}}</p>
                                                    </div>
                                                </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>

                                <form  method="post" action="savecomment">
                                    {{csrf_field()}}
                                    <h3 class="subtitle">Leave a Reply</h3>
                                    <div class="row">
                                        <div class="col col-m-12 col-t-12 col-d-6">
                                            <input type="hidden" name="post_id" value="{{$post_id}}"/>
                                            <input type="hidden" name="url" value="devop2"/>
                                            <div class="group-val">
                                                <input type="text" name="name" placeholder="Your Name" />
                                            </div>
                                            <div class="group-val">
                                                <input type="text" name="email" placeholder="Your Email" />
                                            </div>
                                            <div class="group-val ct-gr">
                                                <textarea name="comment" placeholder="Message"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn_animated" ><span class="circle">Send Message</span></button>
                                        </div>
                                    </div>
                                </form>

                                @if(Session::has('success'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>Success!</strong> {{Session::get('success')}}
                                    </div>
                                @endif

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <footer class="align-center">
            <div class="socials">
                <a target="blank" href="https://www.facebook.com"><i class="icon ion ion-social-facebook"></i></a>
                <a target="blank" href="https://github.com"><i class="icon ion ion-social-github"></i></a>

            </div>
            <div class="copy">© 2018 onetech. all rights reserved.</div>
        </footer>

    </div>

</div>

<!-- jQuery Scripts -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/masonry.pkgd.js"></script>
<script src="js/imagesloaded.pkgd.js"></script>
<script src="js/masonry-filter.js"></script>
<script src="js/magnific-popup.js"></script>
<script src="js/jquery.mb.YTPlayer.js"></script>
<script src="js/particles.js"></script>
<script src="js/typed.js"></script>

<!-- Main Scripts -->
<script src="js/main.js"></script>

</body>
</html>