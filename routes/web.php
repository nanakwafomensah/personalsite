<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('blog');
});
Route::get('/about', function () {
    return view('home');
});

Route::get('blog',['as'=>'blog','uses'=>'devopController@index']);
Route::get('devop1',['as'=>'devop1','uses'=>'devopController@devop1']);
Route::get('devop2',['as'=>'devop2','uses'=>'devopController@devop2']);
Route::get('devop3',['as'=>'devop3','uses'=>'devopController@devop3']);
Route::get('devop4',['as'=>'devop4','uses'=>'devopController@devop4']);

Route::post('savecomment',['as'=>'savecomment','uses'=>'commentController@save']);



Route::get('insert-ajax/{name}/{phone}/{email}/{subject}/{message}', 'mailController@send');