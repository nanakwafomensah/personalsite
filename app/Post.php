<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $primaryKey = 'post_id';

    public $incrementing = false;
    protected $fillable=['post_id','postcategory'];

    public function comments(){
        return $this->hasMany('App\Comment');
    }
}
