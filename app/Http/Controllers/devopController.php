<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class devopController extends Controller
{
    //
    public function index(){
        return view('blog');
    }
    public function devop1(){

        return view('blog/devop/devop1')->with(['post_id'=>1]);
    }
    public function devop2(){
        return view('blog/devop/devop2')->with(['post_id'=>2]);
    }
    public function devop3(){
        return view('blog/devop/devop3')->with(['post_id'=>3]);
    }
    public function devop4(){
        return view('blog/devop/devop4')->with(['post_id'=>4]);
    }
}
