<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Session;
class commentController extends Controller
{
    //
    public function save(Request $request){

        $comment= new Comment();
        $comment->post_post_id= $request->post_id;
        $comment->comment= $request->comment;
        $comment->name= $request->name;
        $comment->email=$request->email;
        $comment->save();
        
        Session::flash('success','Thanks, your message is sent successfully. We will contact you shortly!');

        return redirect($request->url);
    }
}
