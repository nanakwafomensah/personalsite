<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $primaryKey = 'post_post_id';

    public $incrementing = false;
    protected $fillable=['post_post_id','comment','name','email'];
}
